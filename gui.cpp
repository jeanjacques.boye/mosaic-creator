#include "gui.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QPushButton>
#include <QRadioButton>
#include <QSlider>

GUI::GUI(QWidget *parent)
    : QMainWindow(parent)
{
    QWidget *zoneCentrale = new QWidget;
    setCentralWidget(zoneCentrale);
    mc = new MosaicCreator();
    mainlayout = new QVBoxLayout;
    paletteReady = false;
    pictureReady = false;
    mosaicReady = false;
    jpegCompression = 50;
    monoMode = false;


    QWidget * line1= new QWidget;
    QHBoxLayout * line1Layout = new QHBoxLayout;
    QPushButton * openDirBtn = new QPushButton("Choisir dossier");
    QPushButton * openPicBtn = new QPushButton("Sélectionner une photo");
    QPushButton * saveBtn = new QPushButton("Exporter le resultat");
    line1Layout->addWidget(openDirBtn);
    line1Layout->addWidget(openPicBtn);
    line1->setLayout(line1Layout);

    QWidget * line2= new QWidget;
    QHBoxLayout * line2Layout = new QHBoxLayout;


    QSlider * zoneSize = new QSlider(Qt::Horizontal);
    QSlider * pixelSize = new QSlider(Qt::Horizontal);
    zoneSize->setMinimum(1);
    zoneSize->setMaximum(100);
    pixelSize->setMinimum(1);
    pixelSize->setMaximum(100);
    QLabel * zoneSizeValue = new QLabel;
    QLabel * pixelSizeValue = new QLabel;
    QLabel * exportResolution = new QLabel("Choisir une image pour obtenir une résolution de l'export");


    QPushButton * createMosaicBtn = new QPushButton("Créer la mosaique !");
    currentPictureLabel = new QLabel("Choisir une photo pour l'afficher");
    MosaicPictureLabel = new QLabel("Mosaique non créée");
    line2Layout->addWidget(currentPictureLabel);
    line2Layout->addWidget(MosaicPictureLabel);
    line2->setLayout(line2Layout);

    QLabel * engineStatus = new QLabel("Statut du processeur");
    pBar = new QProgressBar();
    pBar->setMaximum(100);

    QWidget * lineModeSelect = new QWidget;
    QHBoxLayout * lineModeSelectLayout = new QHBoxLayout;
    QRadioButton * colorBtn = new QRadioButton("Couleur",lineModeSelect);
    QRadioButton * bwBtn = new QRadioButton("Noir&Blanc",lineModeSelect);
    lineModeSelectLayout->addWidget(new QLabel("Choisir un mode de couleur !"));
    lineModeSelectLayout->addWidget(colorBtn);
    lineModeSelectLayout->addWidget(bwBtn);

    connect(colorBtn,&QRadioButton::toggled,[=](bool test){
       if(test){
           monoMode=false;
       } else{
           monoMode=true;
       }
       if (mosaicReady){
           QImage mosaic;
           if(monoMode){
              mosaic = mosaicPictures.second;
           }
           else{
              mosaic = mosaicPictures.first;
           }
           setMosaicPicture(mosaic);
       }
    });
    colorBtn->setChecked(true);
    lineModeSelect->setLayout(lineModeSelectLayout);

    mainlayout->addWidget(line1);
    mainlayout->addWidget(lineModeSelect);
    mainlayout->addWidget(createMosaicBtn);
    mainlayout->addStretch(10);
    mainlayout->addWidget(line2);
    mainlayout->addStretch(10);
    mainlayout->addWidget(saveBtn);
    mainlayout->addWidget(engineStatus);
    mainlayout->addWidget(pBar);

    QWidget * lineZoneSize = new QWidget;
    QHBoxLayout * lineZoneSizeLayout = new QHBoxLayout;
    lineZoneSizeLayout->addWidget(new QLabel("Taille du quadrillage de la photo choisi :"));
    lineZoneSizeLayout->addWidget(zoneSizeValue);
    lineZoneSizeLayout->addWidget(zoneSize);
    lineZoneSize->setLayout(lineZoneSizeLayout);

    QWidget * linePixelSize = new QWidget;
    QHBoxLayout * linePixelLayout = new QHBoxLayout;
    linePixelLayout->addWidget(new QLabel("Taille des images :"));
    linePixelLayout->addWidget(pixelSizeValue);
    linePixelLayout->addWidget(pixelSize);
    linePixelSize->setLayout(linePixelLayout);



    mainlayout->addWidget(lineZoneSize);
    mainlayout->addWidget(linePixelSize);
    mainlayout->addWidget(exportResolution);

    zoneCentrale->setLayout(mainlayout);


    connect(mc,&MosaicCreator::statusUpdated,engineStatus,&QLabel::setText);
    connect(mc,&MosaicCreator::taskCompletionUpdated,pBar,&QProgressBar::setValue);
    connect(mc,&MosaicCreator::paletteCreated,[&](){
        paletteReady = true;
    });

    connect(zoneSize,&QSlider::valueChanged,[=](int value){
        mc->setZoneSize(value);
        zoneSizeValue->setText(QString::number(value));
        if(pictureReady){
            int h = pixelSize->value()*currentPicture.height()/value;
            int w = pixelSize->value()*currentPicture.width()/value;
            QString s = "Résolution de la mosaique : "+QString::number(w)+" x "+QString::number(h);
            exportResolution->setText(s);
        }

    });
    connect(pixelSize,&QSlider::valueChanged,[=](int value){
        mc->setPixelColorSize(value);
        pixelSizeValue->setText(QString::number(value));
        if(pictureReady){
            int h = value*currentPicture.height()/zoneSize->value();
            int w = value*currentPicture.width()/zoneSize->value();
            QString s = "Résolution de la mosaique : "+QString::number(w)+" x "+QString::number(h);
            exportResolution->setText(s);
        }
    });
    connect(openDirBtn,&QPushButton::clicked,this,&GUI::openPaletteDir);
    connect(openPicBtn,&QPushButton::clicked,this,&GUI::selectPhoto);
    connect(createMosaicBtn,&QPushButton::clicked,this,&GUI::createMosaic);
    connect(saveBtn,&QPushButton::clicked,this,&GUI::saveMosaic);

    pixelSize->setValue(50);
    zoneSize->setValue(10);




}

GUI::~GUI()
{

}

void GUI::openPaletteDir()
{
    QString s = QFileDialog::getExistingDirectory(this,"Selectionner le dossier");
    mc->importPhotos(QDir(s));
}

void GUI::selectPhoto()
{
    QString s = QFileDialog::getOpenFileName(this, "Ouvrir un fichier", QString(), "Images (*.png *.gif *.jpg *.jpeg *.bmp)");
    setCurrentPicture(QImage(s));
    mc->findingColors(&currentPicture);
    pictureReady = true;
    mosaicReady = false;
}

void GUI::createMosaic()
{
    if(pictureReady && paletteReady){
        mosaicPictures = mc->photoToMosaic(&currentPicture);
        QImage mosaic;
        if(monoMode){
           mosaic = mosaicPictures.second;
        }
        else{
           mosaic = mosaicPictures.first;
        }
        setMosaicPicture(mosaic);
        mosaicReady = true;
    }
    else{
        QMessageBox::information(this,"Information","Il faut choisir un dossier et une photo avant de créer une mosaique !");

    }

}

void GUI::saveMosaic()
{
    if(mosaicReady){
        QString filename = QFileDialog::getSaveFileName(this,"Enregistrer la mosaique","export.jpg","Images (*.jpg)");
        QImage mosaic;
        if(monoMode){
           mosaic = mosaicPictures.second;
        }
        else{
           mosaic = mosaicPictures.first;
        }
        mosaic.save(filename,nullptr,jpegCompression);
    }
    else{
        QMessageBox::information(this,"Information","Il faut créer avant de l'exporter");
    }
}

void GUI::setCurrentPicture(QImage value)
{
    currentPicture = value;
    currentPictureLabel->setPixmap(QPixmap::fromImage(value.scaledToHeight(800)));
}

void GUI::setMosaicPicture(QImage value)
{
    mosaicPicture = value;
    MosaicPictureLabel->setPixmap(QPixmap::fromImage(value.scaledToHeight(800)));
}
