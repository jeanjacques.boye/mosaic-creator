#ifndef GUI_H
#define GUI_H

#include "mosaiccreator.h"

#include <QLabel>
#include <QMainWindow>
#include <QProgressBar>
#include <QBoxLayout>

class GUI : public QMainWindow
{
    Q_OBJECT
public:
    GUI(QWidget *parent = nullptr);
    ~GUI();


    void setCurrentPicture(QImage value);
    void setMosaicPicture(QImage value);

public slots:
    void openPaletteDir();
    void selectPhoto();
    void createMosaic();
    void saveMosaic();



private:
    QImage  currentPicture;
    QImage mosaicPicture;
    QPair<QImage,QImage>  mosaicPictures;
    QLabel * currentPictureLabel;
    QLabel * MosaicPictureLabel;
    QProgressBar * pBar;
    QVBoxLayout * mainlayout;
    MosaicCreator * mc;
    int jpegCompression;
    bool paletteReady;
    bool pictureReady;
    bool mosaicReady;
    bool monoMode;
};

#endif // GUI_H
