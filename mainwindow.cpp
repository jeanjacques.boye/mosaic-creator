#include "mainwindow.h"

#include <QGridLayout>
#include <QLabel>
#include <QMenuBar>
#include <QFileDialog>
#include <QMessageBox>
#include <QPixmap>
#include <QVector3D>
#include <QColorDialog>
#include <iostream>



using namespace std;
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QWidget *zoneCentrale = new QWidget;
    setCentralWidget(zoneCentrale);
    jpegCompression = 20;
    mMosaicCreator = new MosaicCreator();
    QLabel * mosaicStatus = new QLabel(mMosaicCreator->status());
    connect(mMosaicCreator,&MosaicCreator::statusUpdated,mosaicStatus,&QLabel::setText);
    connect(mMosaicCreator,&MosaicCreator::resultReady,this,&MainWindow::printPicture);

    //============================================================================//
    //                      Création du menu Fichier                              //
    //============================================================================//
    QMenu * menuFichier = menuBar()->addMenu("&Fichier");

    //============================================================================//
    //                           Fichier -> Ouvrir                                //
    //============================================================================//
    QAction *actionOuvrir = new QAction("Ouvrir une nouvelle image", this);
    menuFichier->addAction(actionOuvrir);
    connect(actionOuvrir, SIGNAL(triggered()), this, SLOT(OuvrirImage()));
    //============================================================================//
    //               Création et positionnement des Bouttons                      //
    //============================================================================//
    m_BoutonQuitter = new QPushButton ("TaDa !");
    m_BoutonOuvrir = new QPushButton ("Choisir Dossier palette");
    m_BoutonColor = new QPushButton("Choisir couleur");

    imagePrint = new QLabel("Hello World!");


    mainlayout  = new QVBoxLayout;
    QHBoxLayout * firstLine = new QHBoxLayout;
    firstLine->addWidget(m_BoutonOuvrir);
    firstLine->addWidget(m_BoutonQuitter);
    QWidget * line1= new QWidget;
    line1->setLayout(firstLine);
    zoneCentrale->setLayout(mainlayout);
    mainlayout->addWidget(line1);
    mainlayout->addWidget(imagePrint);
    mainlayout->addWidget(m_BoutonColor);
    mainlayout->addSpacing(5);
    mainlayout->addWidget(mosaicStatus);
    //connect(m_BoutonOuvrir, SIGNAL(clicked()), this, SLOT(OuvrirImage()));
    connect(m_BoutonOuvrir, SIGNAL(clicked()), this, SLOT(OuvrirDossier()));
    connect(m_BoutonQuitter,&QPushButton::clicked,this,&MainWindow::selectPicture);
    connect(m_BoutonColor,&QPushButton::clicked,this,&MainWindow::colorSelected);
    pBar = new QProgressBar(this);
    pBar->setMaximum(100);
    mainlayout->addWidget(pBar);
    pBar->show();
    connect(mMosaicCreator,&MosaicCreator::taskCompletionUpdated,pBar,&QProgressBar::setValue);

}

void MainWindow::getFichierImage(QString FichierImage){

    FichierImage = *m_FichierImage;
}

void MainWindow::renameFiles(QDir dir)
{
    int indice =0;
    for (QString s : m_currentDir.entryList()) {
        if (s=="." or s==".."){
            continue;
        }
        indice++;
        QImage * img =  new QImage(dir.filePath(s));
        long w= img->width();
        long h = img->height();
        long r1=0,r2=0,r3=0,r4 = 0;
        long g1=0,g2=0,g3=0,g4 = 0;
        long b1=0,b2=0,b3=0,b4 = 0;
        long size=0;
        for (int i = 0 ; i<w/2 ;i=i+1) {
            for (int j=0 ; j<h/2 ;j=j+1) {
                size++;
                r1+=img->pixelColor(i,j).red();
                g1+=img->pixelColor(i,j).green();
                b1+=img->pixelColor(i,j).blue();
            }
        }
        for (int i = w/2 ; i<w ;i=i+1) {
            for (int j=h/2 ; j<h ;j=j+1) {
                size++;
                r2+=img->pixelColor(i,j).red();
                g2+=img->pixelColor(i,j).green();
                b2+=img->pixelColor(i,j).blue();
            }
        }
        for (int i = w/2 ; i<w ;i=i+1) {
            for (int j=0 ; j<h/2 ;j=j+1) {
                size++;
                r3+=img->pixelColor(i,j).red();
                g3+=img->pixelColor(i,j).green();
                b3+=img->pixelColor(i,j).blue();
            }
        }
        for (int i = 0 ; i<w/2 ;i=i+1) {
            for (int j=h/2 ; j<h ;j=j+1) {
                size++;
                r4+=img->pixelColor(i,j).red();
                g4+=img->pixelColor(i,j).green();
                b4+=img->pixelColor(i,j).blue();
            }
        }
        QVector3D q1 = QVector3D(r1/size,g1/size,b1/size);
        QVector3D q2 = QVector3D(r2/size,g2/size,b2/size);
        QVector3D q3 = QVector3D(r3/size,g3/size,b3/size);
        QVector3D q4 = QVector3D(r4/size,g4/size,b4/size);
        int lol = 1000;
        if(q1.distanceToPoint(q2) > lol ||q1.distanceToPoint(q3)> lol ||q1.distanceToPoint(q4)> lol ||q2.distanceToPoint(q3)> lol ||q2.distanceToPoint(q4)> lol ||q3.distanceToPoint(q4) > lol){
            continue;
        }

        int r = r1+r2+r3+r4;
        int b = b1+b2+b3+b4;
        int g = g1+g2+g3+g4;

        imgMap[MyVector3D(r/size,g/size,b/size)]=*img;
        pBar->setValue(indice);
    }

}

QVector3D MainWindow::colorVector(QColor color)
{
    QVector3D vect = QVector3D(color.red(),color.green(),color.blue());
    return vect;
}

void MainWindow::createPalette()
{
    QVector<int> satValue = {240,180,110,55};
    QVector<int> lumValue = {245,205,165,125,85,45,30};
    QVector<int> hueValue = {0,20,40,60,80,100,120,140,160,180,200,220,240};
    QImage result = QImage(1400,650,QImage::Format_ARGB32_Premultiplied);
    int sat,lum,hue;
    int xRel;
    int yRel;
    QImage toPrint;
    QColor pixelColor;
    for(int k = 0 ; k<4 ;k++){

        sat = satValue[k];
        for(int j = 0 ; j<7 ;j++){

            lum = lumValue[j];
            xRel =j*50 + k*350;
            for(int i = 0 ; i<13 ;i++){

                hue = hueValue[i];
                yRel = i*50;
                QColor oColor;
                oColor.setHsv(hue,sat,lum);
                QColor currentColor = QColor(oColor.red(),oColor.green(),oColor.blue());
                toPrint = getColorImage(currentColor,50);
                for (int x = 0;x<50 ;x++ ) {
                    for (int y = 0;y<50 ; y++) {
                        pixelColor = toPrint.pixelColor(x,y);
                        result.setPixelColor(x+xRel,y+yRel,pixelColor);
                    }
                }
            }
        }

    }
    imagePrint->setPixmap(QPixmap::fromImage(result));


}

QImage MainWindow::getColorImage(QColor a, int size)
{
    MyVector3D color = MyVector3D(a.red(),a.green(),a.blue());
    float dist = 10000000;
    QImage s;
    for (MyVector3D d : imgMap.keys()) {
        if (color.distanceToPoint(d)<dist){
            dist = color.distanceToPoint(d);
            s=imgMap[d];
        }
    }
    /*
    if(dist>70){
        return getColorImage(QColor(0,0,0),size);
    }*/
    return s.scaledToWidth(size);
}

void MainWindow::createImgPixel(QImage img)
{
    int ratio = 15;
    hFinal = img.size().height()/ratio;
    wFinal = img.size().width()/ratio;

    QVector<QColor> temp;
    for (int i = 0;i<hFinal ;i++ ) {
        temp.clear();
        for (int j =0;j<wFinal ;j++ ) {
            int r=0,g=0,b=0;

            for (int x = 0 ; x<ratio ;x++) {
                for (int y = 0 ; y<ratio ;y++) {
                    int x1 = x+j*ratio;
                    int y1 = y+i*ratio;
                    r+=img.pixelColor(x1,y1).red();
                    g+=img.pixelColor(x1,y1).green();
                    b+=img.pixelColor(x1,y1).blue();
                }
            }

            int size = ratio*ratio;
            qDebug() << QVector3D(r/size,g/size,b/size);
            temp.append(QColor(r/size,g/size,b/size));
        }
        imagePixels.append(temp);
    }
    finalResult();


}

void MainWindow::finalResult()
{
    int indice=0;
    pBar->setValue(indice);
    pBar->setMaximum(imagePixels.length()*imagePixels[0].length());
    int ratio = 30;
    QImage * result =new QImage(ratio*wFinal,ratio*hFinal,QImage::Format_ARGB32_Premultiplied);
    for (int j =0 ; j<wFinal ; j++) {
        for (int i=0;i<hFinal ; i++ ) {
            QColor cur = imagePixels[i][j];
            for (int x=0;x<ratio ;x++ ) {
                for(int y=0;y<ratio ;y++){
                    int xRel = j*ratio;
                    int yRel = i*ratio;
                    QImage curImg = getColorImage(cur,ratio);
                    result->setPixelColor(x+xRel,y+yRel,curImg.pixelColor(x,y));
                }
            }
           indice++;
           pBar->setValue(indice);
        }
    }

    imagePrint->setPixmap(QPixmap::fromImage(*result));
    result->save("test.jpg");

}

void MainWindow::OuvrirImage(){

    m_FichierImage = new QString();
    *m_FichierImage = QFileDialog::getOpenFileName(this, "Ouvrir un fichier", QString(), "Images (*.png *.gif *.jpg *.jpeg *.bmp)");
    //QMessageBox::information(this, "Fichier", "Vous avez sélectionné :\n" + *m_FichierImage);
    imageIn = new QImage(*m_FichierImage);
    imageOut = new QImage(imageIn->scaledToHeight(300));
    imagePrint->setPixmap(QPixmap::fromImage(*imageOut));

}

void MainWindow::OuvrirDossier()
{
    /*
    QString s = QFileDialog::getExistingDirectory(this,"Selectionner le dossier");

    m_currentDir = QDir(s);

    pBar = new QProgressBar(this);
    pBar->setMaximum(m_currentDir.entryList().length()-2);
    mainlayout->addWidget(pBar);
    pBar->show();

    renameFiles(m_currentDir);
    qDebug() << imgMap.keys().length();
    createPalette();
    */
    QString s = QFileDialog::getExistingDirectory(this,"Selectionner le dossier");

    mMosaicCreator->importPhotos(QDir(s));




}

void MainWindow::colorSelected()
{
    QColor a = QColorDialog::getColor(Qt::white,this,"Select a color");
    MyVector3D color = MyVector3D(a.red(),a.green(),a.blue());
    float dist = 10000000;
    QImage s;
    for (MyVector3D d : imgMap.keys()) {
        if (color.distanceToPoint(d)<dist){
            dist = color.distanceToPoint(d);
            s=imgMap[d];
        }
    }
    //imageIn = new QImage(m_currentDir.filePath(s));
    //imageOut = new QImage(imageIn->scaledToHeight(200));
    imagePrint->setPixmap(QPixmap::fromImage(s.scaledToHeight(200)));

}

void MainWindow::selectPicture()
{
    m_FichierImage = new QString();
    *m_FichierImage = QFileDialog::getOpenFileName(this, "Ouvrir un fichier", QString(), "Images (*.png *.gif *.jpg *.jpeg *.bmp)");
    //QMessageBox::information(this, "Fichier", "Vous avez sélectionné :\n" + *m_FichierImage);
    QImage result = mMosaicCreator->photoToMosaic(new QImage(*m_FichierImage));
    printPicture(&result);
}

void MainWindow::printPicture(QImage *img)
{
    imagePrint->setPixmap(QPixmap::fromImage(*img));
    img->save("Resultat.jpg");
}



MainWindow::~MainWindow()
{
}

