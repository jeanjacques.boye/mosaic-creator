#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "myvector3d.h"

#include <QDir>
#include <QLabel>
#include <QMainWindow>
#include <QPushButton>
#include <QProgressBar>
#include <QVBoxLayout>
#include "mosaiccreator.h"



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


    void getFichierImage(QString FichierImage);
    void renameFiles(QDir dir);
    QVector3D colorVector(QColor color);
    void createPalette();
    QImage getColorImage(QColor a, int size);
    QVector<QVector<QColor>> imagePixels;
    void createImgPixel(QImage img);
    void finalResult();


public slots :
    void OuvrirImage();
    void OuvrirDossier();
    void colorSelected();
    void selectPicture();
    void printPicture(QImage * img);


private:
    QPushButton *m_BoutonQuitter;
    QPushButton *m_BoutonOuvrir;
    QPushButton *m_BoutonColor;
    QString *m_FichierImage;
    QDir m_currentDir;
    QImage * imageIn;
    QImage * imageOut;
    QLabel * imagePrint;
    QMap<MyVector3D,QImage> imgMap;
    QProgressBar * pBar;
    QVBoxLayout * mainlayout;
    int hFinal;
    int wFinal;
    MosaicCreator * mMosaicCreator;
        int jpegCompression;

};
#endif // MAINWINDOW_H
