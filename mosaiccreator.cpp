#include "mosaiccreator.h"

#include <QFuture>
#include <QFutureWatcher>
#include <QImage>
#include <QMap>
#include <QPainter>

#include <qtconcurrentmap.h>



MosaicCreator::MosaicCreator(QObject * parent) : QObject(parent)
{
    mStatus = "Empty palette";
    mTaskCompletion = 100;
    colorScale = 256;
    zoneSize = 10;
    pixelColorSize=50;
    resultAvailable=false;

}

MosaicCreator::~MosaicCreator()
{

}

QPair<QImage,QImage> MosaicCreator::photoToMosaic(QImage *img)
{
    findingColors(img);
    QList<pairpair> list;
    QList<QPair<QPair<QPoint,int>,QMap<int,QImage*>>> monoList;

    for(auto aze :mPictureColors ){
        list.append(pairpair(aze,mBasePalette));
    }
    for (auto qwe:mPictureMonoColors){
        monoList.append(QPair<QPair<QPoint,int>,QMap<int,QImage*>>(qwe,mMonoPalette));
    }

    int hFinal = img->size().height()/zoneSize;
    int wFinal = img->size().width()/zoneSize;
    QImage result =QImage(pixelColorSize*wFinal,pixelColorSize*hFinal,QImage::Format_ARGB32_Premultiplied);
    QImage monoResult =QImage(pixelColorSize*wFinal,pixelColorSize*hFinal,QImage::Format_Grayscale8);
    QPainter p(&result);
    QPainter pm(&monoResult);

    setStatus("Finding the right colors...");

    mPixels = QtConcurrent::blockingMappedReduced<QList<pairPI>>(list,&MosaicCreator::addToPixelListMapper,&MosaicCreator::addToPixelList);
    setStatus("Drawing the mosaic...");
    int value =0;
    int maxVal=mPixels.length();
    for(QPair<QPoint,int> pixel: mPixels){
                QPoint offset =QPoint(pixel.first.x()*pixelColorSize,pixel.first.y()*pixelColorSize);
                QImage curImg = (mBasePalette[pixel.second].second->scaledToWidth(pixelColorSize));
                p.drawImage(offset,curImg);
                value++;
                setTaskCompletion(100*value/maxVal);
    }
    setStatus("Mosaic is ready !");


    QVector<pairPI> ixel = QtConcurrent::blockingMappedReduced<QList<pairPI>>(monoList,&MosaicCreator::addToPixelMonoMapper,&MosaicCreator::addToPixelMono);
    setStatus("Drawing the mosaic...");
    int monovalue =0;
    int monomaxVal=ixel.length();
    for(pairPI pixel: ixel){
                QPoint offset =QPoint(pixel.first.x()*pixelColorSize,pixel.first.y()*pixelColorSize);
                QImage curImg = (mMonoPalette[pixel.second]->scaledToWidth(pixelColorSize));
                pm.drawImage(offset,curImg);
                monovalue++;
                setTaskCompletion(100*monovalue/monomaxVal);
    }
    setStatus("Mosaic is ready !");
    return QPair<QImage,QImage>(result,monoResult);
}

void MosaicCreator::addToPixelList(QList<pairPI> &pixelList,const pairPI &data){
    pixelList.append(data);
}

void MosaicCreator::addToPixelMono(QList<QPair<QPoint,int>> &monoList,const QPair<QPoint,int> &data ){
    monoList.append(data);
}

pairPI MosaicCreator::addToPixelListMapper(const pairpair &data){
    MyVector3D point1 = data.first.second;
    QList<pairColorImage> baseList = data.second;
    int iter=0;
    int dist = 100000;
    int ans=0;
    for(pairColorImage i : baseList){
        MyVector3D point2 = i.first;
        int distTest =point1.distanceToPoint(point2);
        if(distTest<dist){
            dist = distTest;
            ans = iter;
        }
        iter++;
    }
    return pairPI(data.first.first,ans);
}

QPair<QPoint,int> MosaicCreator::addToPixelMonoMapper(const QPair<pairPI,QMap<int,QImage*>> &data){
    int greyColor = data.first.second;
    QMap<int,QImage*> map = data.second;
    int dist=0;
    int ans = -1;
    while (ans ==-1){
        if(dist==0 && map.contains(greyColor)){
            ans=greyColor;
        }
        else if(map.contains(greyColor+dist)){
            ans=greyColor+dist;
        }
        else if(map.contains(greyColor-dist)){
            ans=greyColor-dist;
        }
        dist++;
    }
    return QPair<QPoint,int>(data.first.first,ans);
}


void MosaicCreator::createPalette()
{
    setStatus("Creating monochrome palette...");

    mMonoPalette.clear();
    mMonoPalette = QtConcurrent::blockingMappedReduced(mPhotoList,&MosaicCreator::findImageMonoColor,&MosaicCreator::addToMonoPalette);
    setStatus("Creating Color palette...");
    mBasePalette.clear();
    mBasePalette = QtConcurrent::blockingMappedReduced(mPhotoList,&MosaicCreator::findImageColor,&MosaicCreator::addToBasePalette);
    setStatus("Base Palette created");
    emit paletteCreated();

}
void MosaicCreator::findingColors(QImage *img)
{
    setStatus("Analysing picture");
    int hFinal = img->size().height()/zoneSize;
    int wFinal = img->size().width()/zoneSize;
    mPictureColors.clear();
    mPictureMonoColors.clear();
    int maxVal = hFinal*wFinal;
    int indice = 0;
    for (int i = 0;i<hFinal ;i++ ) {
        for (int j =0;j<wFinal ;j++ ) {
            int r=0,g=0,b=0;
            int l=0;
            for (int x = 0 ; x<zoneSize ;x++) {
                for (int y = 0 ; y<zoneSize ;y++) {
                    int x1 = x+j*zoneSize;
                    int y1 = y+i*zoneSize;
                    QColor a = img->pixelColor(x1,y1);
                    r+=a.red();
                    g+=a.green();
                    b+=a.blue();
                    l+=(a.red()*11+a.blue()*5+a.green()*16);
                }
            }
            int size = zoneSize*zoneSize;
            indice++;
            mPictureColors.append(QPair<QPoint,MyVector3D>(QPoint(j,i),MyVector3D(r/size,g/size,b/size)));
            mPictureMonoColors.append(QPair<QPoint,int>(QPoint(j,i),(l/size)));
            setTaskCompletion(100*indice/maxVal);
        }

    }

}

void MosaicCreator::addToBasePalette(QList<pairColorImage> &basePalette,const pairColorImage &p) //reduce1
{
    basePalette.append(p);
}

void MosaicCreator::addToMonoPalette(QMap<int,QImage *> &monoPalette,const QPair<int,QImage*> &p){
    monoPalette.insert(p.first,p.second);

}


pairColorImage MosaicCreator::findImageColor(const QImage &photo) //map1
{
    int w = photo.width();
    int h = photo.height();
    int size = 0;
    long r=0,g=0,b=0;
    for (int i = 0 ; i<w ;i++) {
        for (int j=0 ; j<h ;j++) {
            r+=photo.pixelColor(i,j).red();
            g+=photo.pixelColor(i,j).green();
            b+=photo.pixelColor(i,j).blue();
            size++;
        }
    }
    if(size==0){size++;}
    return pairColorImage(MyVector3D(r/size,g/size,b/size),new QImage(photo));
}

QPair<int,QImage *> MosaicCreator::findImageMonoColor(const QImage &photo){
    int w = photo.width();
    int h = photo.height();
    int size = 0;
    long l=0;
    for (int i = 0 ; i<w ;i++) {
        for (int j=0 ; j<h ;j++) {
            QColor a = photo.pixelColor(i,j);
            l+=(a.red()*11+a.blue()*5+a.green()*16);
            size++;
        }
    }
    if(size==0){size++;}
    return QPair<int,QImage*>(l/size,new QImage(photo));
}



int MosaicCreator::taskCompletion() const
{
    return mTaskCompletion;
}

int MosaicCreator::getBasePaletteSize() const
{
    return mBasePalette.length();
}


void MosaicCreator::importPhotos(QDir dir)
{
    QString s;
    QStringList list = dir.entryList();
    setTaskCompletion(0);
    setStatus("Loading pictures for palette...");
    mPhotoList.clear();
    for (int i = 0; i < list.size(); ++i) {
        s = list.at(i);
        if (s=="." or s==".."){
            continue;
        }
        mPhotoList.append(QImage(dir.filePath(s)));
        setTaskCompletion(i/list.size());
    }
    setStatus("Pictures loaded !");
    createPalette();

}

void MosaicCreator::setMonoScale(int value)
{
    monoScale = value;
}

int MosaicCreator::getPixelColorSize() const
{
    return pixelColorSize;
}

void MosaicCreator::setPixelColorSize(int value)
{
    pixelColorSize = value;
}

int MosaicCreator::getZoneSize() const
{
    return zoneSize;
}

void MosaicCreator::setZoneSize(int value)
{
    zoneSize = value;
}


QList<pairColorImage> MosaicCreator::getBasePalette() const
{
    return mBasePalette;
}

void MosaicCreator::setTaskCompletion(int value)
{
    mTaskCompletion = value;
    emit taskCompletionUpdated(value);
}

QString MosaicCreator::status() const
{
    return mStatus;
}

void MosaicCreator::setStatus(const QString &value)
{
    mStatus = value;
    emit statusUpdated(mStatus);
}
