#ifndef MOSAICCREATOR_H
#define MOSAICCREATOR_H

#include <QDir>
#include "myvector3d.h"
#include <QImage>

typedef QPair<MyVector3D,QImage*> pairColorImage;
typedef QMap<MyVector3D,QImage*> palette;
typedef QPair<QPoint,int> pairPI;
typedef QPair<QPair<QPoint, MyVector3D>,QList<pairColorImage>> pairpair;

class MosaicCreator : public QObject
{
    Q_OBJECT

public:
    MosaicCreator(QObject * parent = nullptr);
    ~MosaicCreator();
    QString status() const;
    int taskCompletion() const;
    int getBasePaletteSize() const;
    QPair<QImage,QImage> photoToMosaic(QImage *img);

    QList<pairColorImage> getBasePalette() const;


    int getZoneSize() const;
    void setZoneSize(int value);

    int getPixelColorSize() const;
    void setPixelColorSize(int value);

    void setMonoScale(int value);
    void findingColors(QImage *img);

private:
    static pairColorImage findImageColor(const QImage &image);  //map1
    static QPair<int, QImage *> findImageMonoColor(const QImage &photo);
    static void addToBasePalette(QList<pairColorImage> &basePalette,const pairColorImage &p); //reduce1
    static void addToMonoPalette(QMap<int, QImage *> &monoPalette, const QPair<int, QImage *> &p);

    static pairPI addToPixelListMapper(const pairpair &data);
    static QPair<QPoint, int> addToPixelMonoMapper(const QPair<QPair<QPoint, int>, QMap<int, QImage *> > &data);
    static void addToPixelList(QList<pairPI> &pixelList, const pairPI &data);
    static void addToPixelMono(QList<QPair<QPoint, int> > &monoList, const QPair<QPoint, int> &data);

    void setStatus(const QString &value);
    void setTaskCompletion(int value);

    void createPalette();

public slots:
    void importPhotos(QDir dir);
signals:
    QString statusUpdated(QString status);
    int taskCompletionUpdated(int percent);
    QImage * resultReady(QImage * result);
    void paletteCreated();

private:
    QList<QImage> mPhotoList;
    QString mStatus;
    int mTaskCompletion;
    QList<pairColorImage> mBasePalette;
    QMap<int,QImage *> mMonoPalette;
    QList<pairPI> mPixels;
    int colorScale;
    int monoScale;
    QList<QPair<QPoint,MyVector3D>> mPictureColors;
    QList<QPair<QPoint,int>> mPictureMonoColors;
    QList<QPair<QPoint,int>> mPicturePixels;
    int zoneSize; //indique la taille des zones servant à découper la photo d'origine
    int pixelColorSize;
    QImage * result;
    bool resultAvailable;










};

#endif // MOSAICCREATOR_H
