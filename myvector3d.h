#ifndef MYVECTOR3D_H
#define MYVECTOR3D_H

#include <QVector3D>
class MyVector3D : public QVector3D
{
public:
    MyVector3D();
    MyVector3D(float x,float y,float z);

    friend bool operator<(MyVector3D const a ,MyVector3D const b){
        if (a==b){return false;}
        QVector3D zero = QVector3D(0,0,0);
        if(a.distanceToPoint(zero)<b.distanceToPoint(zero)){return true;}
        else if(a.distanceToPoint(zero)>b.distanceToPoint(zero)){return false;}
        else{
            if(a.x()<b.x()){return true;}
            else if(a.x()>b.x()){return false;}
            else{
                if(a.y()<b.y()){return true;}
                else if(a.y()>b.y()){return false;}
                else{
                    if(a.z()<b.z()){return true;}
                    else if(a.z()>b.z()){return false;}
                    else{return false;}
                }
            }
        }
    };

};

#endif // MYVECTOR3D_H
